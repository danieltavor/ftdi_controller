
import json
from flask import Flask, jsonify, request
import time
import threading
from ftdi_controller import FT232Controller

# Reading server configurations from the JSON file
with open('server_config.json', 'r') as config_file:
    config = json.load(config_file)

# Server Configurations defaults
SERVER_PORT = config.get('port', "8080")
SERVER_HOST = config.get('host', '0.0.0.0')
DEBUG_MODE = config.get('debug', True)
USE_RELOADER = config.get('use_reloader', False)
CONNECTION_CHECK_INTERVAL = config.get('connection_check_interval', 5)

app = Flask(__name__)

controller = FT232Controller()

def check_and_reconnect():
    while True:
        if not controller.check_connection():
            controller.reconnect_device()
        time.sleep(CONNECTION_CHECK_INTERVAL)

# Start the background thread for checking the device connection
device_monitor_thread = threading.Thread(target=check_and_reconnect, daemon=True)
device_monitor_thread.start()

def device_disconnected_response():
    return jsonify({'error': 'FTDI device is disconnected'}), 503

@app.route('/', methods=['GET'])
def index():
    return "Welcome to the FTDI IO Controller Server!"

@app.route('/set_switch', methods=['POST'])
def set_switch():
    if not controller.device_connected:
        return device_disconnected_response()

    data = request.json
    switch_name = data.get('switch')  # Expecting 'SW0' or 'SW1'
    state = data.get('state')  # Expecting 0 or 1

    if switch_name not in ['SW0', 'SW1'] or state not in [0, 1]:
        return jsonify({'error': 'Invalid switch name or state'}), 400

    controller.set_pin_state(switch_name, state)
    return jsonify({'message': f'{switch_name} set to {state}'}), 200

@app.route('/read_status', methods=['GET'])
def read_status():
    if not controller.device_connected:
        return device_disconnected_response()

    controller.read_pin_states()
    bus_status = controller.adbus_pins
    status = {
        bus_status["ADBUS2"]["name"]: bus_status["ADBUS2"]["state"],
        bus_status["ADBUS3"]["name"]: bus_status["ADBUS3"]["state"]
    }
    return jsonify(status), 200

if __name__ == '__main__':
    app.run(debug=DEBUG_MODE, host=SERVER_HOST, port=SERVER_PORT, use_reloader=USE_RELOADER)
