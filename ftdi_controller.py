import ftd2xx
import time
import json
from enum import Enum

class PinType(Enum):
    CBUS = "CBUS"
    ADBUS = "ADBUS"

class ADBUS(Enum):
    ADBUS0 = "ADBUS0"
    ADBUS1 = "ADBUS1"
    ADBUS2 = "ADBUS2"
    ADBUS3 = "ADBUS3"

class CBUS(Enum):
    CBUS0 = "CBUS0"
    CBUS1 = "CBUS1"
    CBUS2 = "CBUS2"
    CBUS3 = "CBUS3"

class FT232Controller:
    BITMODE_CBUS = 0x20  # CBUS bitbang mode
    BITMODE_SYNCBB = 0x04  # Synchronous BitBang mode for ADBUS
    DEFAULT_IO_CONFIG_FILE = "io_config.json"

    def __init__(self, config_file = None):

        if config_file == None:
            self.config_file = self.DEFAULT_IO_CONFIG_FILE
        else:
            self.config_file = config_file

        self.device = None
        self.device_info = None
        self.cbus_pins = {}
        self.adbus_pins = {}
        self.serial_number = None
        self.load_config(self.config_file)
        self.device_connected = self.open_device()

    def open_device(self):
        try:
            if self.serial_number:
                # Convert serial number to bytes and open the specified device
                serial_number_bytes = self.serial_number.encode('utf-8')
                self.device = ftd2xx.openEx(serial_number_bytes)
            else:
                # Open the first available device if no serial number is provided
                self.device = ftd2xx.open(0)
            self.device_info = self.device.getDeviceInfo()
            return True
        
        except ftd2xx.ftd2xx.DeviceError as e:
            print(f"Device Error: {e}")
            # raise Exception(f"Device Error: {e}")
            return False

    @staticmethod
    def find_all_devices():
        return ftd2xx.listDevices()

    def load_config(self, file_path):
        try:
            with open(file_path, 'r') as file:
                config = json.load(file)
                self.cbus_pins = config.get('CBUS', {})
                self.adbus_pins = config.get('ADBUS', {})
                self.serial_number = config.get('serial_number')
                self.pin_name_map = {v['name']: k for k, v in self.adbus_pins.items()}
        except FileNotFoundError:
            print(f"Config file not found: {file_path}")
        except json.JSONDecodeError:
            print(f"Error decoding JSON from file: {file_path}")


    def set_pin_state(self, custom_name, state):
        pin_name = self.pin_name_map.get(custom_name, custom_name)

        if pin_name in self.adbus_pins:
            self.adbus_pins[pin_name]['state'] = state
            self.update_device_state('ADBUS')
        elif pin_name in self.cbus_pins:
            self.cbus_pins[pin_name]['state'] = state
            self.update_device_state('CBUS')
        else:
            print(f"Invalid pin name: {custom_name}")


    def read_pin_states(self):
        # Read the state of all pins
        pin_states = self.device.getBitMode()
        self.update_pin_states('CBUS', pin_states)
        self.update_pin_states('ADBUS', pin_states)


    def update_pin_states(self, pin_type, pin_states):
        if pin_type == 'CBUS':
            for pin_name, pin_info in self.cbus_pins.items():
                if pin_info['direction'] == 'input':
                    pin_index = int(pin_name[-1])
                    pin_info['state'] = (pin_states >> pin_index) & 1
        elif pin_type == 'ADBUS':
            for pin_name, pin_info in self.adbus_pins.items():
                if pin_info['direction'] == 'input':
                    pin_index = int(pin_name[-1])
                    pin_info['state'] = (pin_states >> pin_index) & 1

    def update_device_state(self, pin_type):
        if pin_type == 'CBUS':
            value = 0x00
            for pin_name, pin_info in self.cbus_pins.items():
                if pin_info['direction'] == 'output':
                    value |= (pin_info['state'] << int(pin_name[-1]))  # Assuming the pin name ends with a digit
            self.set_cbus_pins(value)
        elif pin_type == 'ADBUS':
            value = 0x00
            for pin_name, pin_info in self.adbus_pins.items():
                if pin_info['direction'] == 'output':
                    value |= (pin_info['state'] << int(pin_name[-1]))  # Assuming the pin name ends with a digit
            self.set_adbus_pins(value)


    def set_cbus_pins(self, value):
        if self.device:
            self.device.setBitMode(0x00, self.BITMODE_CBUS)  # Enable CBUS bitbang mode
            self.device.write(bytes([value & 0x0F]))

    def set_adbus_pins(self, value):
        if self.device:
            self.device.setBitMode(0xFF, self.BITMODE_SYNCBB)  # Enable ADBUS bitbang mode
            self.device.write(bytes([value]))


    def close(self):
        if self.device:
            self.device.close()
            self.device = None

    def check_connection(self):
        try:
            if self.device:
                self.device.getDeviceInfo()
        except ftd2xx.ftd2xx.DeviceError:
            print("Device disconnected.")
            self.device_connected = False
            self.device = None
            return False
        return True

    def reconnect_device(self):
        while not self.device:
            print("Attempting to reconnect...")
            try:
                self.device_connected = self.open_device()
                time.sleep(2)
            except ftd2xx.ftd2xx.DeviceError:
                pass


if __name__ == '__main__':
    controller = FT232Controller()

    if not controller.device_connected:
        print("Couldn't open the device")

    devices = FT232Controller.find_all_devices()
    print(devices)  # List of connected FTDI devices

    while True:

        # Check for device connection
        if not controller.check_connection():
            time.sleep(1)
            controller.reconnect_device()
        else:
            time.sleep(1)
            controller.set_pin_state("SW0", 0)
            time.sleep(1)
            controller.set_pin_state("SW0", 1)
            time.sleep(1)
            controller.set_pin_state("SW1", 0)
            time.sleep(1)
            controller.set_pin_state("SW1", 1)
            controller.read_pin_states()
            print(controller.adbus_pins)

    controller.close()
