
# FTDI FT232 IO Controller

## Overview
The `FT232Controller` class is designed to interface with FTDI FT232 series USB-to-serial converter chips. It allows for the control and monitoring of the chip's GPIO pins via CBUS and ADBUS pin types. This Python class works in conjunction with a configuration file (`io_config.json`) to define the behavior of the GPIO pins.

## Dependencies
- `Python 3.9`
- `ftd2xx`: A Python library for communicating with FTDI devices.
- `time`: Standard Python module for time-related tasks.
- `json`: Standard Python module for JSON manipulation.

## Installation (Offline)
```bat
python -m venv venv
.\venv\Scripts\activate
pip install --no-index --find-links whl_packages -r requirements.txt
```

## Configuration JSON Format
The configuration file (`io_config.json`) defines the properties of the GPIO pins. It includes settings for both CBUS and ADBUS pins, such as direction, state, and custom names.

Example:
```json
{
    "serial_number": null,
    "CBUS": {
        "CBUS0": {"name": "CBUS0", "direction": "output", "state": 0, "initial_state": 0},
        ...
    },
    "ADBUS": {
        "ADBUS0": {"name": "SW0", "direction": "output", "state": 0, "initial_state": 0},
        ...
    }
}
```

## Class Documentation

### `FT232Controller`

#### `__init__(self, config_file=None)`
Initializes the controller. If `config_file` is not specified, it defaults to `"io_config.json"`.

#### `open_device(self)`
Opens the FTDI device based on the serial number provided in the config file. Returns `True` if successful, otherwise `False`.

#### `find_all_devices()`
Static method that returns a list of all connected FTDI devices.

#### `load_config(self, file_path)`
Loads the GPIO pin configuration from a JSON file.

#### `set_pin_state(self, custom_name, state)`
Sets the state of a GPIO pin by its custom name.

#### `read_pin_states(self)`
Reads the current state of all GPIO pins.

#### `update_pin_states(self, pin_type, pin_states)`
Updates the internal state of the pins based on the `pin_type` (either 'CBUS' or 'ADBUS').

#### `update_device_state(self, pin_type)`
Updates the device's state for all pins of the specified `pin_type`.

#### `set_cbus_pins(self, value)`
Sets the state of CBUS pins.

#### `set_adbus_pins(self, value)`
Sets the state of ADBUS pins.

#### `close(self)`
Closes the connection to the FTDI device.

#### `check_connection(self)`
Checks the connection to the FTDI device. Returns `True` if connected, otherwise `False`.

#### `reconnect_device(self)`
Attempts to reconnect to the FTDI device if the connection is lost.

### Enums: `PinType`, `ADBUS`, `CBUS`
Defines constants for pin types and names.

## Usage Example

```python
from FT232Controller import FT232Controller

# Initialize the controller
controller = FT232Controller()

# Check if device is connected
if not controller.device_connected:
    print("Couldn't open the device")

# List all connected devices
devices = FT232Controller.find_all_devices()
print(devices)

# Control loop
while True:
    if not controller.check_connection():
        controller.reconnect_device()
    else:
        # Example: Toggle a switch
        controller.set_pin_state("SW0", 0)
        time.sleep(1)
        controller.set_pin_state("SW0", 1)

    # Read the state of all pins
    controller.read_pin_states()
    print(controller.adbus_pins)

# Close the controller
controller.close()
```

## Conclusion
The `FT232Controller` class provides an easy-to-use interface for controlling FTDI FT232 series chips. By utilizing the configuration file, users can customize the behavior of the GPIO pins to suit their specific needs.
